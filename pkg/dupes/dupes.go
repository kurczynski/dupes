package dupes

import (
	"fmt"
	"gitlab.com/kurczynski/dupes/pkg/cksum"
	"gitlab.com/kurczynski/dupes/pkg/fileutil"
	"sync"
	"time"
)

type Workers struct {
	count int
	ckSum *cksum.CkSum
}

type workerInfo struct {
	wg        sync.WaitGroup
	mutex     sync.RWMutex
	tasks     chan fileutil.Info
	output    map[string][]fileutil.Info
	doneCount int
}

func New(nWorkers int) *Workers {
	d := Workers{
		count: nWorkers,
		ckSum: cksum.New("SHA256"),
	}

	return &d
}

func (w *Workers) createWorkers(info *workerInfo) {
	for i := 0; i < w.count; i++ {
		go w.worker(info)
	}
}

func (w *Workers) worker(info *workerInfo) {
	for i := range info.tasks {
		ckSum, err := w.ckSum.Sum(&i)

		if err != nil {
			fmt.Printf("Could not calculate checksum for %s: %s\n", i.Path, err)
		}

		info.mutex.Lock()
		info.output[ckSum] = append(info.output[ckSum], i)
		info.doneCount++
		info.mutex.Unlock()

		info.wg.Done()
	}
}

func (w *Workers) GetDupes(fGroups map[int64][]fileutil.Info) map[string][]fileutil.Info {
	info := workerInfo{
		wg:    sync.WaitGroup{},
		mutex: sync.RWMutex{},
		// TODO: Parameterize the number of channels
		tasks:  make(chan fileutil.Info, 1_000_000),
		output: make(map[string][]fileutil.Info),
	}

	w.createWorkers(&info)

	fCount := 0

	go func() {
		for info.doneCount < fCount {
			fmt.Printf("\rProcessed %d files", info.doneCount)
			time.Sleep(time.Second)
		}
	}()

	for _, v := range fGroups {
		for _, i := range v {
			fCount++
			// Only calculate the checksum for files that are of the same size
			if len(v) > 1 {
				info.wg.Add(1)
				info.tasks <- i
			}
		}
	}

	fmt.Printf("\nDone\n")

	close(info.tasks)

	info.wg.Wait()

	return getDupeCkSums(info.output)
}

func getDupeCkSums(files map[string][]fileutil.Info) map[string][]fileutil.Info {
	output := make(map[string][]fileutil.Info)

	for k, v := range files {
		if len(v) > 1 {
			output[k] = v
		}
	}

	return output
}

func PrintDupes(files map[string][]fileutil.Info) {
	for hash, fList := range files {
		fmt.Printf("%s - %d duplicates\n", hash, len(fList))

		for _, f := range fList {
			fmt.Printf("%s\n", f.Path)
		}
	}

}
