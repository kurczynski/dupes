package fileutil

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
)

const roPerm = 0o0444

type FileUtils interface {
	GroupFilesBySize(string) map[int64][]Info
}

type FileUtil struct{}

type Info struct {
	Path string
	Size int64
}

func ListFiles(root string) {
	i := int64(0)

	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		fmt.Println(info.Name())

		i++

		return nil
	})

	log.Printf("File count: %d\n", i)

	if err != nil {
		panic(err)
	}
}

func (FileUtil) GroupFilesBySize(root string) map[int64][]Info {
	fmt.Printf("Looking for files in path: %s\n", root)

	fGroups := map[int64][]Info{}
	fCount := 0

	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Printf("An error occurred while searching for files: %+v\n", err)

			return nil
		} else if info != nil {
			log.Printf("Found file %s\n", info.Name())

			fMode := uint32(info.Mode().Perm())

			if !info.Mode().IsRegular() {
				return nil
			} else if fMode&roPerm < roPerm {
				log.Printf("%s for file: %s/%s\n", os.ErrPermission, path, info.Name())

				return nil
			}

			fSize := info.Size()

			fGroups[fSize] = append(fGroups[fSize], Info{Path: path, Size: fSize})

			fCount++
		}

		return nil
	})

	if err != nil {
		fmt.Printf("An error was encountered while searching for files: %s\n", err)
	}

	fmt.Printf("Found %d files\n", fCount)

	return fGroups
}
