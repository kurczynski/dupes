package cksum

import (
	"crypto/sha256"
	"fmt"
	"gitlab.com/kurczynski/dupes/pkg/fileutil"
	"hash"
	"io"
	"os"
)

type CkSum struct {
	hashName string
}

func New(hashName string) *CkSum {
	return &CkSum{
		hashName: hashName,
	}
}

func chooseHash(hashName string) (hash.Hash, error) {
	switch hashName {
	case "SHA256":
		return sha256.New(), nil
	}

	return nil, fmt.Errorf("Unknown hash type \"%s\"", hashName)
}

// Calculate the checksum of the given file.
func (s *CkSum) Sum(file *fileutil.Info) (string, error) {
	f, err := os.Open(file.Path)

	defer func() {
		err = f.Close()

		if err != nil {
			fmt.Println(err)
		}
	}()

	if err != nil {
		return "", err
	}

	hasher := s.NewHasher()

	if _, err := io.Copy(hasher, f); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", hasher.Sum(nil)), nil
}

// Creates a new hasher using the algorithm specified in the struct.
func (s *CkSum) NewHasher() hash.Hash {
	hasher, err := chooseHash(s.hashName)

	if err != nil {
		panic(err)
	}

	return hasher
}
