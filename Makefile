MAIN := 'cmd/dupes/main.go'
BIN := 'dupes'

.PHONY: all
all: test build

.PHONY: test
test:
	go test -cover ./...

.PHONY: build
build:
	go build -o $(BIN) $(MAIN)

.PHONY: clean
clean:
	go clean ./...
