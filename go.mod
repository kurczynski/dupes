module gitlab.com/kurczynski/dupes

go 1.13

require (
	github.com/gobuffalo/packr/v2 v2.4.0
	github.com/spf13/pflag v1.0.3
)
