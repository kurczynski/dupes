# dupes
Early stages of an application to find duplicate files on a file system. I was motivated to write this because the tools
that I used to search for duplicate files were painfully slow and lacked status information during a run. Knowing the
status of a run is nice when a large disk is being scanned. This project attempts to accomplish two things:

1. Find duplicate files on a file system in a quick and efficient way.
2. Provide a codebase that is easy to understand.

Although the first item is IO bound (more specifically, sys calls to `fopen()` and `fclose()`), the use of file
heuristics may improve performance.

## Build
Run unit tests with coverage and build binary:
```shell script
make
```

Build binary:
```shell script
make build
```

Run unit tests with coverage:
```shell script
make test
```

## Run
To check the current directory for duplicate files:
```shell script
./dupes
```

To check a specific directory for duplicate files:
```shell script
./dupes --path /some/specific/path
```

## How it works
_In progress..._

## License
[GPLv3](./LICENSE)
