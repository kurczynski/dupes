package main

import (
	"fmt"
	"github.com/spf13/pflag"
	"gitlab.com/kurczynski/dupes/pkg/dupes"
	"gitlab.com/kurczynski/dupes/pkg/fileutil"
	"io/ioutil"
	"log"
	"os"
	"runtime"
)

func main() {
	logging := pflag.BoolP(
		"log",
		"l",
		false,
		"enable logging",
	)

	path := pflag.StringP(
		"path",
		"p",
		".",
		"path to check for duplicate files",
	)

	numCpus := runtime.NumCPU()

	nWorkers := pflag.IntP(
		"worker-count",
		"w",
		numCpus,
		"number of workers to create",
	)

	pflag.Parse()

	if !*logging {
		log.SetOutput(ioutil.Discard)
	}

	_, err := os.Stat(*path)

	if err != nil {
		log.Printf("Cannot access %s\n", *path)
		os.Exit(1)
	}

	d := dupes.New(*nWorkers)

	fileUtil := fileutil.FileUtil{}

	fList := fileUtil.GroupFilesBySize(*path)
	dupeList := d.GetDupes(fList)

	dupes.PrintDupes(dupeList)

	fmt.Println("Done!")
}
