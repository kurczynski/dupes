package logger

import (
	"io"
	"io/ioutil"
	"log"
	"os"
)


func Logger(verbose bool) (*log.Logger) {
	var logFlag int
	var logOut io.Writer

	if verbose {
		logFlag = log.Ldate | log.Ltime | log.Lshortfile
		logOut = os.Stdout
	} else {
		logFlag = 0
		logOut = ioutil.Discard
	}

	return log.New(logOut, "General Logger:\t", logFlag)
}
